a = 10;
"""
if (si)
else (caso contrario)
elif (if else)
"""
if a <= 20:
    print(a, " es menor a 20", sep="\n")
elif a >= 20 and a<=50:
    print(a, "es menor a 50 y mayor a 20 ")
elif 20 >= a <= 50:
    print(a, "es menor a 50 y mayor a 20 ")
else:
    print(a , " es mayor a 50")

# bucles

"""
for 
while
"""
print("For")
for i in range(10):
    print(i)

i = 0
print("\nWhile")
while i <= 10:
    print(i)
    i += 1

