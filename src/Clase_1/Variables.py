# variables

"""
int (enteros
float (decimales)
str (string)
bool (booleanos)
"""

# Declaramos una variable de tipo int
a = 10
b: int = 10

# float
c = 10.2
d: float = 15.2

# string
e = 'hola'
f: str = "hola"

# booleanos
bool_a = True or False
bool_b: bool = False and True

# entradas desde el teclado
# entrada por teclado
try:
    in_a: int = int(input("Ingrese algo: "))
    in_b: int = int(input("Ingrese algo: "))
    print(in_a + in_b)
    variable_a = 10
except:
    print("el tipo de dato no corresponde")
# salida por teclado
