# python

1. Activar el entorno virtual
```bash
.\venv\Scripts\activate
```

1. Entrada y salida de datos 
    - para poder ingresar valores por teclado se usa `input` pero esto lo que hace es retornar un 'string'
    ```python
        input_a = input("Ingrese algo por teclado: ")
        print(type(input_a))
        # la salida sera `<class 'str'>`
    ```
    - entonces si yo quiero convertir el str a un int se debe utilizar lo que viene a ser el casting de datos es decir convertir una variable de un tipo de dato a otro
    ```python
        input_a: int = int(input("Ingrese algo por teclado: "))
        print(type(input_a))
        # la salida sera <class 'int'>
    ```
2. Variables
   - una variable podríamos ejemplificarlo como un contenedor que guarda algo en su interior, básicamente representan un espacio en memoria en el cual se guardan algo y esta variable apunta a ese espacio en memoria que guarda un valor al que le corresponde un typo de dato
   - En python tenemos los siguientes tipos de datos:
     - int `numeros enteros`
     - float `numeros decimales`
     - str `Cadenas de texto`
     - bool `booleanos`
   - se puede declarar una variable de la siguiente manera
    ```commandline
     <nombre varianle> = <valor>
     <nombre>: <tipo de dato> = <valor>
    ```
    - Ejemplo: un entero
    ```python
    a = 10
    b: int = 10
    ```
3. Estructuras condicinales y Estructuras repetitivas
   - if...else...elif:
   python admite las condiciones matemáticas habituales:
      - igualdad `a == b`
      - no es igual `a != b`
      - menor que `a < b`
      - mayor que `a > b`
      - mayor o igual que `a >= b`
      - menor o igual que `a <= b`
    
    ```python
    a: int = 10;
    if a <= 20:
        print(a, " es menor a 20", sep="\n")
    elif a >= 20 and a<=50:
        print(a, "es menor a 50 y mayor a 20 ")
    elif 20 >= a <= 50:
        print(a, "es menor a 50 y mayor a 20 ")
    else:
        print(a , " es mayor a 50")
    ```
   - Estructuras repetitivas (for, while):
   